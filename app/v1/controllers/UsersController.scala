package v1.controllers

import javax.inject._

import play.api.mvc._
import play.api.libs.json._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

import v1.dao._
import v1.models._

@Singleton
class UsersController @Inject() (
  userRepository: UserRepository,
  cc: ControllerComponents
) extends AbstractController(cc) {

  val userForm: Form[User] = Form {
      mapping(
        "id" -> optional(longNumber),
        "name" -> nonEmptyText,
        "age" -> number.verifying(min(0), max(140)),
        "is_deleted" -> number.verifying(min(0), max(1))
      )(User.apply)(User.unapply)
    }

  def get = Action.async {
    userRepository.list().map { user => Ok(Json.toJson(user)).as("application/json") }
  }

  def post = Action.async {
    implicit request => {
      val body = request.body.asJson
      body match {
        case Some(json) =>
          userRepository.create(User(
            None,
            (json \ "name").asOpt[String].getOrElse(""),
            (json \ "age").asOpt[Int].getOrElse(20),
            0
          )).map { res => res match {
            case 1 => Ok(Json.obj("result" -> true))
            case _ => Ok(Json.obj("result" -> false))
          } }
        case None => Future.successful(Ok(Json.obj("result" -> false)))
      }
    }
  }
}
