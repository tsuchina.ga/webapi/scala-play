package v1.controllers

import javax.inject._

import play.api.mvc._
import play.api.libs.json._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

import v1.dao._
import v1.models._

class UsersIdController @Inject() (
  userRepository: UserRepository,
  cc: ControllerComponents
) extends AbstractController(cc) {

  val userForm: Form[User] = Form {
      mapping(
        "id" -> optional(longNumber),
        "name" -> nonEmptyText,
        "age" -> number.verifying(min(0), max(140)),
        "is_deleted" -> number.verifying(min(0), max(1))
      )(User.apply)(User.unapply)
    }

  def get(id: Long) = Action.async {
    userRepository.findById(id).map { user => Ok(Json.toJson(user)).as("application/json") }
  }

  def put(id: Long) = Action.async {
    implicit request => {
      request.body.asJson match {
        case Some(json) => {
          userRepository.findById(id).map(x => x.get).map { user =>
            userRepository.update(id, User(
              Some(id),
              (json \ "name").asOpt[String].getOrElse(user.name),
              (json \ "age").asOpt[Int].getOrElse(user.age),
              user.is_deleted
            ))
            Ok(Json.obj("result" -> true))
          }
        }
        case None => Future.successful(Ok(Json.obj("result" -> false)))
      }
    }
  }

  def delete(id: Long) = Action.async {
    userRepository.delete(id).map { res => res match {
      case 1 => Ok(Json.obj("result" -> true))
      case _ => Ok(Json.obj("result" -> false))
    } }
  }
}
