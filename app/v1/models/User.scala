package v1.models

import play.api.libs.json._

case class User(
  id: Option[Long],
  name: String,
  age: Int,
  is_deleted: Int
)

object User {
  implicit val userFormat = Json.format[User]
}
