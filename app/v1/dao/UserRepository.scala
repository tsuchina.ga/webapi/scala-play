package v1.dao

import javax.inject.{Inject, Singleton}

import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import v1.models.User

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
{
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private class UsersTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[(Long)]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def age = column[Int]("age")
    def is_deleted = column[Int]("is_deleted")

    def * = (id.?, name, age, is_deleted) <> ((User.apply _).tupled, User.unapply)
  }

  private val users = TableQuery[UsersTable]

  def findById(id: Long): Future[Option[User]] = db.run {
    users.filter(u => u.id === id.bind).result.headOption
  }

  def list(): Future[Seq[User]] = db.run {
    users.filter(u => u.is_deleted === 0).result
  }

  def create(user: User): Future[Int] = db.run {
    users += user
  }

  def update(id: Long, user: User): Future[Int] = db.run {
    users.filter(u => u.id === id).update(user)
  }

  def delete(id: Long): Future[Int] = db.run {
    users.filter(u => u.id === id).map(user => (user.is_deleted)).update(1)
  }
}
