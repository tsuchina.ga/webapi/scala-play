# scala-play

Play2 Frameworkを使ってRESTfulなWebAPIを作ってみる。


## 最初に参考にしたサイト

* [Home - 2.6.x | Play Framework - Build Modern & Scalable Web Apps with Java and Scala](https://www.playframework.com/documentation/2.6.x/Home)
* [Play Framework 2.6 アプリをDocker上で開発する - Qiita](https://qiita.com/kazuyoshikakihara/items/f87b4b377b5844f09614)
* [Home - 2.4.x | Play Framework - Build Modern & Scalable Web Apps with Java and Scala](https://www.playframework.com/documentation/ja/2.4.x/Home)
* [Scala初心者が、Play FrameworkとMySQLで、CRUDでRESTfulなJSON APIを作ってみた（その4） | Recruit Jobs TECHBLOG](https://techblog.recruitjobs.net/development/crud-restful-json-api_on_play-framework_and_mysql_4)
* [ScalaForms - 2.6.x | Play Framework - Build Modern & Scalable Web Apps with Java and Scala](https://www.playframework.com/documentation/2.6.x/ScalaForms)


## Dockerでの開発環境

```
> docker run -d -it --name scala-play -p 9000:9000 -v E:/Projects/tsuchinaga/webapi-test/scala/play:/root hseeberger/scala-sbt
> docker exec -it scala-play bash

$ sbt new playframework/play-scala-seed.g8 --name=scala-play
$ cd scala-play
$ sbt
$ ~ run
```

あとはローカルにあるE:/Projects/tsuchinaga/webapi-test/scala/play/scala-playの中身をいじれば、自動でコンパイルしてくれて、[localhost:9000](http://localhost:9000)で確認できる


## DBアクセスで困ったら

* プロジェクトディレクトリ配下にlibディレクトリはある？
* libディレクトリに「postgresql-42.2.1.jar」のような接続用jarファイルはある？
* `play.api.db._`はimportされてる？
* Injectにdb: Databaseは含まれてる？
